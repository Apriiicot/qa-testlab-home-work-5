package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.asserts.SoftAssert;
import pretashop3.model.ProductData;

import java.util.List;
import java.util.Random;

public class SiteProductPage extends AbstractPageObject {
    public SiteProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[@class='thumbnail product-thumbnail']")
    private List<WebElement> productsOnPage;

    public ProductPage selectRandomProduct(){
        waitForContentLoad(By.xpath("//a[@class='thumbnail product-thumbnail']"));
        productsOnPage.get(new Random().nextInt(productsOnPage.size())).click();
        return new ProductPage(driver);
    }
}
