package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class OrderPage extends AbstractPageObject{
    public OrderPage(WebDriver driver) {
        super(driver);
    }

    public ConfirmationPage fillOrder(){
        WebElement gender = driver.findElement(By.name("id_gender"));
        gender.click();
        WebElement firstname = driver.findElement(By.name("firstname"));
        firstname.sendKeys("Dima");
        WebElement lastName = driver.findElement(By.name("lastname"));
        lastName.sendKeys("N");
        WebElement customerForm = driver.findElement(By.id("customer-form"));
        WebElement email =  customerForm.findElement(By.name("email"));
        email.sendKeys("12421421@gmail.com");
        WebElement aContinue = customerForm.findElement(By.name("continue"));
        aContinue.click();

        WebElement addressLive = driver.findElement(By.name("address1"));
        addressLive.sendKeys("Kiev");

        WebElement postcode = driver.findElement(By.name("postcode"));
        postcode.sendKeys("11111");

        WebElement city = driver.findElement(By.name("city"));
        city.sendKeys("Kiev");

        WebElement buttonConfirm = driver.findElement(By.name("confirm-addresses"));
        buttonConfirm.click();

        WebElement confirmDeliveryOption = driver.findElement(By.name("confirmDeliveryOption"));
        confirmDeliveryOption.click();

        WebElement paymentOption = driver.findElement(By.id("payment-option-1"));
        paymentOption.click();

        WebElement agreeConditions = driver.findElement(By.id("conditions_to_approve[terms-and-conditions]"));
        agreeConditions.click();

        WebElement agreeOrder = driver.findElement(By.xpath("//button[@class='btn btn-primary center-block']"));
        agreeOrder.click();

        return new ConfirmationPage(driver);


    }

}
