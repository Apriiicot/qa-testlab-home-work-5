package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pretashop3.model.ProductData;
import pretashop3.utils.logging.CustomReporter;

public class ProductPage extends AbstractPageObject{
    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//a[contains(text(), 'Подробнее о товаре')]")
    private WebElement detailProduct;

    @FindBy(xpath = "//button[@class='btn btn-primary add-to-cart']")
    private WebElement toBasasket;

    @FindBy(xpath = "//a[contains(text(), 'перейти к оформлению')]")
    private WebElement placingOrder;

    public BascketPage putProductToBascket(ProductData productData){

        waitForContentLoad(By.xpath("//a[contains(text(), 'Подробнее о товаре')]"));
        detailProduct.click();
        productData.setName(driver.findElement(By.className("h1")).getText());
        productData.setPrice(Float.valueOf(driver.findElement(By.xpath("//span[@itemprop='price']")).getAttribute("content")));
        System.out.println(driver.findElement(By.xpath("//span[contains(text(), 'Товары')]")).getText());
        productData.setQty(Integer.valueOf(driver.findElement(By.xpath("//span[contains(text(), 'Товары')]")).getText().replaceAll("[^0-9\\.]+", "")));

        waitForContentLoad(By.xpath("//button[@class='btn btn-primary add-to-cart']"));
        toBasasket.click();
        waitForContentLoad(By.xpath("//a[contains(text(), 'перейти к оформлению')]"));
        placingOrder.click();
        return new BascketPage(driver);
    }

}
