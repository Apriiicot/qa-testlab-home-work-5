package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import pretashop3.model.ProductData;

public class ConfirmationPage extends AbstractPageObject {
    public ConfirmationPage(WebDriver driver) {
        super(driver);
    }

    public ConfirmationPage checkOrderConfirmation(ProductData productData){
        SoftAssert productAssert = new SoftAssert();
        productAssert.assertEquals(driver.findElement(By.xpath(String.format("//span[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '%s')]", productData.getName().toLowerCase()))).getText().toLowerCase(), productData.getName().toLowerCase());
        productAssert.assertEquals(driver.findElement(By.xpath("//div[@class='col-xs-5 text-sm-right text-xs-left']")).getText().replaceAll("[^0-9\\,]+", ""), productData.getPrice());
        productAssert.assertEquals(driver.findElement(By.className("col-xs-2")).getText(), "1");
        productAssert.assertAll();
        return this;

    }

    public void checkProductCountAfterBuy(ProductData productData){
        WebElement s = driver.findElement(By.name("s"));
        s.sendKeys(productData.getName());
        WebElement button = driver.findElement(By.xpath("//i[@class='material-icons search']/.."));
        button.click();

        driver.findElement(By.xpath(String.format("//a[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '%s')]", productData.getName().toLowerCase()))).click();
        driver.findElement(By.xpath("//a[contains(text(), 'Подробнее о товаре')]")).click();
        Assert.assertEquals(Integer.valueOf(driver.findElement(By.xpath("//span[contains(text(), 'Товары')]")).getText().replaceAll("[^0-9\\.]+", "")).intValue(), (productData.getQty() - 1));
    }


}
