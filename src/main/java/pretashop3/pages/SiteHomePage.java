package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class SiteHomePage extends AbstractPageObject {

    private static final String HOME_PAGE_URL = "http://prestashop-automation.qatestlab.com.ua";

    public SiteHomePage(WebDriver driver) {
        super(driver);
        driver.navigate().to(HOME_PAGE_URL);
    }

    @FindBy(xpath = "//div[@id='contact-link']/..")
    private WebElement contactElement;

    @FindBy(xpath = "//a[contains(text(),'Все товары')]")
    private WebElement allProductsButton;

    public void checkMobileDesktopVersion(String browser){
        waitForContentLoad(By.xpath("//div[@id='contact-link']/.."));
        if(browser.equals("mobile")){
            Assert.assertEquals(contactElement.getAttribute("id"), "_mobile_contact_link");
        }else{
            Assert.assertEquals(contactElement.getAttribute("id"), "_desktop_contact_link");
        }
    }

    public SiteProductPage moveToProductPage(){
        waitForContentLoad(By.xpath("//a[contains(text(),'Все товары')]"));
        allProductsButton.click();
        return new SiteProductPage(driver);
    }

}
