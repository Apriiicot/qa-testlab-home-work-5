package pretashop3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;
import pretashop3.model.ProductData;

public class BascketPage extends AbstractPageObject{

    private static final String XPATH_ORDER_BUTTON = "//a[contains(text(), 'Оформление заказа')]";

    public BascketPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = XPATH_ORDER_BUTTON)
    private WebElement orderButton;

    public BascketPage checkProductData(ProductData productData){
        SoftAssert productAssert = new SoftAssert();
        productAssert.assertEquals(driver.findElement(By.xpath(String.format("//a[contains(translate(text(), 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', 'abcdefghijklmnopqrstuvwxyz'), '%s')]", productData.getName().toLowerCase()))).getText().toLowerCase(), productData.getName().toLowerCase());
        productAssert.assertEquals(driver.findElement(By.xpath("//span[@class='product-price']")).getText().replaceAll("[^0-9\\,]+", ""), productData.getPrice());
        productAssert.assertEquals(driver.findElement(By.xpath("//input[@name='product-quantity-spin']")).getAttribute("value"), "1");
        productAssert.assertAll();
        return this;

    }

    public OrderPage pressOrderProduct(){
        waitForContentLoad(By.xpath(XPATH_ORDER_BUTTON));
        orderButton.click();
        return new OrderPage(driver);
    }

    public static void main(String[] args) {
//        Assert.assertTrue(driver.findElements(By.name("q")).getText() > 0);
        String s = "hi 3,78 hi bye";
        String numberOnly = s.replaceAll("[^0-9\\,]+", "");
        double d = Double.parseDouble(numberOnly); //d == 3.78d
        System.out.println(d);
    }

}
