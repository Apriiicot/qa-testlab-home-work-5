package pretashop3.tests;

import org.testng.annotations.Test;
import pretashop3.model.ProductData;
import pretashop3.pages.SiteHomePage;
import pretashop3.utils.Properties;

public class OrderProductTest  extends BaseScript{

    @Test
    public void checkSiteVersion(){
        new SiteHomePage(driver).checkMobileDesktopVersion(browser);
    }

    @Test(dependsOnMethods = "checkSiteVersion")
    public void createOrder(){
        ProductData productData = new ProductData();
        new SiteHomePage(driver)
                .moveToProductPage()
                .selectRandomProduct()
                .putProductToBascket(productData)
                .checkProductData(productData)
                .pressOrderProduct().fillOrder().checkOrderConfirmation(productData).checkProductCountAfterBuy(productData);
    }

}
