package pretashop3.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import pretashop3.utils.logging.EventHandler;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

public abstract class BaseScript {

    private static final String REMOTE_URL_SELENIUM = "http://localhost:4444/wd/hub";
    protected EventFiringWebDriver driver;
    protected String browser;

    private WebDriver getDriver(String browser) {

        switch (browser){
            case "chrome":
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
            case "firefox":
                System.setProperty(
                        "webdriver.gecko.driver",
                        new File(BaseScript.class.getResource("/geckodriver.exe").getFile()).getPath());
                return new FirefoxDriver();
            case "iexplore":
                System.setProperty(
                        "webdriver.ie.driver",
                        new File(BaseScript.class.getResource("/IEDriverServer.exe").getFile()).getPath());
                InternetExplorerOptions ieOptions = new InternetExplorerOptions().destructivelyEnsureCleanSession();
                ieOptions.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);

                return new InternetExplorerDriver(ieOptions);
            case "remote-chrome":
                try {
                    return new RemoteWebDriver(new URL(REMOTE_URL_SELENIUM), new ChromeOptions());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            case "remote-firefox":
                try {
                    return new RemoteWebDriver(new URL(REMOTE_URL_SELENIUM), new FirefoxOptions());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            case "remote-iwxplorer":
                try {
                    return new RemoteWebDriver(new URL(REMOTE_URL_SELENIUM), new InternetExplorerOptions());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            case "mobile":
                try {
                    HashMap<String, String> mobileEmulation = new HashMap<>();
                    mobileEmulation.put("deviceName", "iPhone 6");

                    ChromeOptions options = new ChromeOptions();
                    options.setExperimentalOption("mobileEmulation", mobileEmulation);
                    return new RemoteWebDriver(new URL(REMOTE_URL_SELENIUM),options);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            default:
                System.setProperty(
                        "webdriver.chrome.driver",
                        new File(BaseScript.class.getResource("/chromedriver.exe").getFile()).getPath());
                return new ChromeDriver();
        }
    }

    private EventFiringWebDriver getConfiguredDriver(String browser) {
        EventFiringWebDriver driver = new EventFiringWebDriver(getDriver(browser));
        driver.manage().timeouts()
                       .implicitlyWait(10, TimeUnit.SECONDS)
                       .pageLoadTimeout(15, TimeUnit.SECONDS) ;
        driver.register(new EventHandler());
        return driver;
    }

    @BeforeClass
    @Parameters("browser")
    public void setUp(@Optional("mobile") String browser){
        driver = new EventFiringWebDriver(getDriver(browser));
        driver.register(new EventHandler());

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        this.browser = browser;
    }

    @AfterClass
    public void tearDown() {
//        if (driver != null) {
//            driver.quit();
//        }
    }


}
